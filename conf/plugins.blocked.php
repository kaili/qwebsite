<?php
/**
 * This file disables plugins which are not allowed in 'Q'. It overwrites all
 * other plugin settings, even if the status could be changed (visual) in the
 * extension manager. 
 * 
 * @todo: interaction with extension manager
 *
 * Do not change this file, as it is overwritten on 'Q' upgrades.
 */

/* The farmer plugin is dangerous! 'Q' have "farming" enabled by default, so
 * there is no requirement of it. DO NOT EDIT THIS !!!                    */
$plugins['farmer']            = 0;
// test:
$plugins['showif']            = 0;
